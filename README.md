# Pictures manager Project

![Version](https://img.shields.io/badge/version-0.1-red.svg?cacheSeconds=2592000)
![Python Version](https://img.shields.io/badge/python-3.6-yellow)
[![GitHub license](https://img.shields.io/badge/license-MIT-green)](https://github.com/wyllisMonteiro/image_gallery/blob/master/LICENSE)

### Requirements
Coming soon

### Build and install 
Comming soon

### Documentations
- BACK : in progress
- FRONT : in progress
- DOCKER : in progress

### Contributing
Please read and follow [CONTRIBUTING.md](https://gitlab.com/valmrt77/pictures-manager/-/blob/master/CONTRIBUTING.md)

### Authors
Valentin Moret : valentin.moretpro@gmail.com

Wyllis Monteiro : wyllismonteiro@gmail.com

### License
Please read and follow [LICENSE](https://gitlab.com/valmrt77/pictures-manager/-/blob/master/LICENSE)