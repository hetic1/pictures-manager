# Front

## Check Python version
```sh
$ python3 -V
```

**Required version is 3.6.x**

## Check django version
```sh
$ python3 -m django --version
```

## Run server
```sh
$ python3 manage.py runserver
```
[Intro](https://docs.djangoproject.com/en/3.0/intro/tutorial01/)

