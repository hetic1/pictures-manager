# Code of conduct
As contributors and maintainers of the Pictures manager project, we pledge to respect everyone who contributes by posting issues, updating documentation, submitting pull requests, providing feedback in comments,
and any other activities. Communication through any of Pictures Manager channels (GitHub, Google++) 
must be constructive and never resort to personal attacks, trolling, public or private harassment, insults, or other unprofessional conduct.
We promise to extend courtesy and respect to everyone involved in this project regardless of gender, gender identity, sexual orientation, disability, age, race, ethnicity, religion, or level of experience.
We expect anyone contributing to the Pictures manager project to do the same. If any member of the community violates this code of conduct,
the maintainers of the Pictures manager project may take action, removing issues, comments, and PRs or blocking accounts as deemed appropriate.

Examples of behavior that contributes to creating a positive environment include:

Using welcoming and inclusive language
Being respectful of differing viewpoints and experiences
Gracefully accepting constructive criticism
Focusing on what is best for the community
Showing empathy towards other community members
Examples of unacceptable behavior by participants include:

The use of sexualized language or imagery and unwelcome sexual attention or advances
Trolling, insulting/derogatory comments, and personal or political attacks
Public or private harassment
Publishing others' private information, such as a physical or electronic address, without explicit permission
Other conduct which could reasonably be considered inappropriate in a professional setting
If you are subject to or witness unacceptable behavior, or have any other concerns, please contact us.

26/05/2020.